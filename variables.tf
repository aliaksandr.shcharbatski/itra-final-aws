variable "aws_region" {
  default = "eu-west-1"
}

variable "cluster-name" {
  default = "itra-terraform-eks-demo"
  type    = string
}
