# Final task (AS)

>Все пункты задания выполнять с использование terraform и AWS.
>
>1. Создать VPC с 4 подсетями, две из которых публичные, две приватные
>
>2. В публичных подсетях развернуть ALB или CLB
>
>3. Создать EKS cluster (использовать для нодов - EC2 Node Group, с автоскейлингом)
>
>4. Развернуть в EKS Nginx поду и организовать доступ к ней через созданный в пункте 2 балансировщик
>

Устанавливаем необходимые утилиты

```
sudo apt install awscli
aws configure
AWS Access Key ID [None]: YOUR_AWS_ACCESS_KEY_ID
AWS Secret Access Key [None]: YOUR_AWS_SECRET_ACCESS_KEY
Default region name [None]: eu-west-1
Default output format [None]: json


Access key id  AKIAWUV5XXXXXQ

Secret access key  Ev6uQ5VLwTeqNt/XXXXXY9ID9Gcg4Cr

echo 'export LBC_VERSION="v2.4.1"' >>  ~/.bash_profile
echo 'export LBC_CHART_VERSION="1.4.1"' >>  ~/.bash_profile
.  ~/.bash_profile

curl -L https://git.io/get_helm.sh | bash -s -- --version v3.8.2
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /home/lively/bin
```

terraform apply разворачивает кластер eks, vpc, 2 приватных и 2 публичных подсети, назначает security groups для обеспечения доступности между сервером kubernetes и воркер нодами, добавляет iam role для доступа eks к aws. по итогу получаем данные для подключения к kubernetes серверу, редактируем файлы (из terraform outputs)
```
nano config_map_aws_auth.yml
nano ~/.kube/config
```
применяем config map
```
kubectl apply -f config_map_aws_auth.yml
```
Проверяем что есть подключенные воркер ноды
```
kubectl get nodes
```
объявляем переменные для дальнейшего использования (версии)
```
echo 'export LBC_VERSION="v2.4.1"' >>  ~/.bash_profile
echo 'export LBC_CHART_VERSION="1.4.1"' >>  ~/.bash_profile
.  ~/.bash_profile
```
Разворачиваем aws-load-balancer-controller
```
eksctl utils associate-iam-oidc-provider \
    --region eu-west-1 \
    --cluster itra-terraform-eks-demo \
    --approve

curl -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/${LBC_VERSION}/docs/install/iam_policy.json
aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file://iam_policy.json

eksctl delete iamserviceaccount   --cluster itra-terraform-eks-demo   --namespace kube-system   --name aws-load-balancer-controller

eksctl create iamserviceaccount \
  --cluster itra-terraform-eks-demo \
  --namespace kube-system \
  --name aws-load-balancer-controller \
  --attach-policy-arn arn:aws:iam::456737153693:policy/AWSLoadBalancerControllerIAMPolicy \
  --override-existing-serviceaccounts \
  --approve


kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"

kubectl get crd

helm repo add eks https://aws.github.io/eks-charts

helm upgrade -i aws-load-balancer-controller \
    eks/aws-load-balancer-controller \
    -n kube-system \
    --set clusterName=itra-terraform-eks-demo \
    --set serviceAccount.create=false \
    --set serviceAccount.name=aws-load-balancer-controller \
    --set image.tag="${LBC_VERSION}" \
    --version="${LBC_CHART_VERSION}"

kubectl -n kube-system rollout status deployment aws-load-balancer-controller
```
в конце разворачиваем nginx по заранее подготовленному yml
```
kubectl apply -f nginx.yml

kubectl get ingress/ingress-nginx -n nginx-ns
```
скриншоты
![img](img/img1.png)
![img](img/img2.png)
![img](img/img3.png)
![img](img/img4.png)
![img](img/img5.png)
![img](img/img6.png)
![img](img/img7.png)
![img](img/img8.png)
![img](img/img9.png)
![img](img/img10.png)